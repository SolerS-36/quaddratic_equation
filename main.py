from flask import Flask, request, render_template
from flask_restful import Api
from api import PostApi
from quaddratic_equation import solve_quaddratic_equation, CalculationsError


app = Flask(__name__)


api = Api(app)
api.add_resource(PostApi, '/api')


@app.route("/", methods=['GET', 'POST'])
def index():
    if request.method == 'POST':
        try:
            a = float(request.form.get('value_a'))
            b = float(request.form.get('value_b'))
            c = float(request.form.get('value_c'))
            try:
                data = solve_quaddratic_equation(a, b, c)
            except CalculationsError as err:
                return render_template('index.html',msg=str(err), coefficient={'a': a, 'b': b, 'c': c}, title='Решение квадратных уравнений')
            if len(data) == 2:
                return render_template('index.html', data=data, coefficient={'a': a, 'b': b, 'c': c}, title='Решение квадратных уравнений')
            elif len(data) == 1:
                return render_template('index.html', x=data[0], coefficient={'a': a, 'b': b, 'c': c}, title='Решение квадратных уравнений')
        except Exception as err:
            return render_template('index.html',msg=str(err), title='Решение квадратных уравнений')

    else:
        return render_template('index.html', title='Решение квадратных уравнений')


app.debug = True
if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8000)