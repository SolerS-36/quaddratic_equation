from typing import Any, Dict, Mapping
from flask import request
from flask_restful import Resource
from quaddratic_equation import solve_quaddratic_equation

class APIException(Exception):
    pass


class PostApi(Resource):
    def post(self) -> Dict[str, Any]:
        """Принимает значения для a, b и c в виде json. Решает квадратное уравнение и возвращает результат в виде json."""
        try:
            incoming_data: Mapping[str, Any] = request.get_json()
            outgoing_data = []
            if incoming_data.keys() != {'a', 'b', 'c'}:
                raise APIException("Полученные данные неполные.")
            outgoing_data = solve_quaddratic_equation(float(incoming_data['a']), float(incoming_data['b']), float(incoming_data['c']))
            return {"success": True, "data": outgoing_data, "error": None}  
        except Exception as err:
            return {"success": False, "data": None, "error": str(err)}

