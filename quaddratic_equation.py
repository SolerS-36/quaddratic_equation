import math
import unittest
from typing import List

class CalculationsError(Exception):
    pass


def solve_discriminant(a: float, b: float, c: float) -> float:
    return b**2 - 4 * a * c


def solve_quaddratic_equation(a: float, b: float, c: float) -> List[float]:
    if a == 0:
        raise CalculationsError("Коэффициент 'a' не может быть равен 0.")
    discriminant = solve_discriminant(a, b, c)
    if discriminant == 0:
        x = -(b / (2 * a))
        return [x]
    elif discriminant > 0:
        sqrt_discriminant = math.sqrt(discriminant)
        x1 = (-b + sqrt_discriminant) / (2 * a)
        x2 = (-b - sqrt_discriminant) / (2 * a)
        return [x1, x2]
    else:
        raise CalculationsError('Уравнение не имеет решения.')


class TestQuaddraticEquation(unittest.TestCase):
    
    def test_solve_quaddratic_equation(self):
        self.assertEqual(solve_quaddratic_equation(0.5, 3.0, 1), [-0.3542486889354093, -5.645751311064591])
        self.assertEqual(solve_quaddratic_equation(-3, 3, 1), [-0.2637626158259733, 1.2637626158259734])
        self.assertEqual(solve_quaddratic_equation(2, 0, 0), [0.0])
        with self.assertRaises(CalculationsError):  # Уравнение не имеет решения.
            solve_quaddratic_equation(0.5, 1, 4)
        with self.assertRaises(CalculationsError):  # Коэффициент 'a' не может быть равен 0.
            solve_quaddratic_equation(0, 3, 1)


if __name__ == '__main__':
    unittest.main()